.. currentmodule:: imod.wq

iMOD-WQ
-------

.. autosummary::
    :toctree: generated/wq
    
    SeawatModel

    TimeDiscretization
    OutputControl
    PreconditionedConjugateGradientSolver
    GeneralizedConjugateGradientSolver
    ParallelKrylovFlowSolver
    ParallelKrylovTransportSolver
    
    BasicFlow
    ConstantHead
    Drainage
    EvapotranspirationTopLayer
    EvapotranspirationLayers
    EvapotranspirationHighestActive
    GeneralHeadBoundary
    LayerPropertyFlow
    RechargeTopLayer
    RechargeLayers
    RechargeHighestActive
    River
    Well
    
    VariableDensityFlow

    AdvectionTVD
    AdvectionMOC
    AdvectionModifiedMOC
    AdvectionHybridMOC
    AdvectionFiniteDifference
    Dispersion
    MassLoading
    TimeVaryingConstantConcentration
