.. currentmodule:: imod.flow

iMODFLOW
--------

.. autosummary::
    :toctree: generated/flow
    
    ImodflowModel
    
    Boundary
    Top
    Bottom
    
    PreconditionedConjugateGradientSolver
    
    ConstantHead
    Drain
    EvapoTranspiration
    GeneralHeadBoundary
    HorizontalHydraulicConductivity
    HorizontalAnisotropy
    HorizontalFlowBarrier
    MetaSwap
    Recharge
    River
    StartingHead
    VerticalHydraulicConductivity
    VerticalAnisotropy
    StorageCoefficient
    SpecificStorage
    Well
