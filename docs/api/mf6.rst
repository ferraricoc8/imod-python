.. currentmodule:: imod.mf6

MODFLOW6
--------

.. autosummary::
    :toctree: generated/mf6

    open_hds
    open_cbc
    read_cbc_headers
    
    Modflow6Simulation
    GroundwaterFlowModel

    StructuredDiscretization
    VerticesDiscretization
    TimeDiscretization

    OutputControl
    Solution
    SolutionPresetSimple
    SolutionPresetModerate
    SolutionPresetComplex
    
    ConstantHead
    Drainage
    Evapotranspiration
    GeneralHeadBoundary
    InitialConditions
    NodePropertyFlow
    Recharge
    River
    SpecificStorage
    StorageCoefficient
    UnsaturatedZoneFlow
    WellDisStructured
    WellDisVertices
