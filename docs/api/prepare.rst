.. currentmodule:: imod.prepare

Prepare model input
-------------------

.. autosummary::
    :toctree: generated/prepare

    Regridder
    LayerRegridder
    Voxelizer

    fill
    laplace_interpolate

    polygonize

    reproject

    rasterize
    gdal_rasterize
    celltable
    rasterize_celltable
    
    zonal_aggregate_polygons
    zonal_aggregate_raster
