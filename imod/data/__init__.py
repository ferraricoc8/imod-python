# from .synthetic import
from .sample_data import (
    ahn,
    fluxes,
    head_observations,
    hondsrug_drainage,
    hondsrug_initial,
    hondsrug_layermodel,
    hondsrug_meteorology,
    hondsrug_river,
    lakes_shp,
    twri_output,
)
